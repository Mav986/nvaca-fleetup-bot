import config
import json

from esipy import EsiApp, EsiClient

esi_app = EsiApp()
app = esi_app.get_latest_swagger
client = EsiClient(
    headers={'User-Agent': config.ESI_USER_AGENT},
    raw_body_only=True
)


def get_id(name, category):
    """
    Get the id of something
    :param name: name of something
    :param category: ESI category the something is part of
    :return: either a json object containing the something id or None
    """
    id_search = app.op['get_search'](search=name, categories=category, strict=True)
    response = client.request(id_search)

    return json.loads(response.raw)


def get_ship_info(ship_name):
    """
    Get the information of a ship
    :param ship_name: ship to retreive info for
    :return: either a json object containing the ship info or None
    """
    ship_id = get_id(ship_name, 'inventory_type')['inventory_type'][0]
    ship_search = app.op['get_universe_types_type_id'](type_id=ship_id)
    response = client.request(ship_search)

    return json.loads(response.raw)


def get_system_info(system_name):
    """
    Get the information of a system
    :param system_name: system to retreive info for
    :return: either a json object containing the system info or None
    """
    system_id = get_id(system_name, 'solar_system').get('solar_system')
    if system_id:
        system_search = app.op['get_universe_systems_system_id'](system_id=system_id[0])
        response = client.request(system_search)
        return json.loads(response.raw)

    return None
